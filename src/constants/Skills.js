export default [
  {
    id: 1,
    img: `${require('../assets/skills/React.svg')}`,
    name: "React",
    color: `#6ab8e7`,
  },
  {
    id: 2,
    img: `${require("../assets/skills/javascript.svg")}`,
    name: "JavaScript",
    color: `#f7df1e`,
  },
  {
    id: 3,
    img: `${require("../assets/skills/html5.svg")}`,
    name: "Html5",
    color: ` #ff5722`,
  },
  {
    id: 4,
    img: `${require("../assets/skills/css.svg")}`,
    name: "Css3",
    color: ` #2196f3`,
  },
  {
    id: 5,
    img: `${require("../assets/skills/gatsbyjs.svg")}`,
    name: "Gatsby",
    color: `#663399`,
  },
  {
    id: 6,
    img: `${require("../assets/skills/sass.svg")}`,
    name: "Sass",
    color: `#cf649a`,
  },
  {
    id: 8,
    img: `${require("../assets/skills/github.svg")}`,
    name: "Github",
    color: `#161614`,
  },
  {
    id: 9,
    img: `${require("../assets/skills/pugjs.svg")}`,
    name: "Pug",
    color: `#4f4644`,
  },
  {
    id: 10,
    img: `${require("../assets/skills/webpack.svg")}`,
    name: "Webpack",
    color: `#1c78c0`,
  },
  {
    id: 11,
    img: `${require("../assets/skills/gulp.svg")}`,
    name: "Gulp",
    color: `#eb4a4b`,
  },
  {
    id: 12,
    img: `${require("../assets/skills/bootstrap.svg")}`,
    name: "Bootstrap",
    color: `#563d7c`,
  },
]
