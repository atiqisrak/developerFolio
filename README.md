
<p align="center">
  <a href="https://www.gatsbyjs.com">
    <img alt="Gatsby" src="https://www.gatsbyjs.com/Gatsby-Monogram.svg" width="60" />
  </a>
</p>
<h1 align="center">
  developerFolio 
</h1>

## 🚀 Quick start

  ```shell
  git clone https://github.com/elalfymohamed/developerFolio.git
  ```

  ```shell
  # Install dependencies
  yarn
  ```

  ```shell
  # Run app
  yarn develop
  ```

  ```shell
  # Build app
  yarn build
  ```
